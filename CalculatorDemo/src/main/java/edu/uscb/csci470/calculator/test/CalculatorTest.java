package edu.uscb.csci470.calculator.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

import edu.uscb.csci470.calculator.Calculator;

public class CalculatorTest {
	
	private Calculator calculatorTestInstance = new Calculator();
	
	private static Logger logger = LoggerFactory.getLogger(Calculator.class);
	
    @Before
	public void setUpBeforeEachTest() {
		
		calculatorTestInstance = new Calculator();
		logger.info("new test instance: " + calculatorTestInstance);
		
	} // end method setUpBeforeEachTest
	
    @Test
	public void testAdd() {
		int value1 = 25;
		int value2 = 10;
		int expectedResult = 35;
		int testResult = calculatorTestInstance.add(value1, value2);
		logger.info("testAdd expected: {}, actual: {}",
				expectedResult, testResult);
		Assert.assertEquals( expectedResult, testResult );
	} // end method testAdd
    
    @Test
	public void testSubtract() {
		int value1 = 25;
		int value2 = 10;
		int expectedResult = 15;
		int testResult = calculatorTestInstance.subtract(value1, value2);
		logger.info("testSubtract expected: {}, actual: {}",
				expectedResult, testResult);
		Assert.assertEquals( expectedResult, testResult );
	} // end method testSubtract
    
    @Test
	public void testMultiply() {
		int value1 = 25;
		int value2 = 10;
		int expectedResult = 250;
		int testResult = calculatorTestInstance.multiply(value1, value2);
		logger.info("testMultiply expected: {}, actual: {}",
				expectedResult, testResult);
		Assert.assertEquals( expectedResult, testResult );
	} // end method testMultiply
    
    @Test
	public void testIntDivide() {
		int value1 = 25;
		int value2 = 10;
		int expectedResult = 2;
		int testResult = calculatorTestInstance.intDivide(value1, value2);
		logger.info("testIntDivide expected: {}, actual: {}",
				expectedResult, testResult);
		Assert.assertEquals( expectedResult, testResult );
	} // end method testMultiply


} // end test class CalculatorTest
