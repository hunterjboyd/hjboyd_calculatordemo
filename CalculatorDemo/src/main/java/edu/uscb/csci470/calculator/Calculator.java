package edu.uscb.csci470.calculator;

/**
* Calculator class that implements arithmetic
* operations as named methods
* @author Hunter Boyd
*/

public class Calculator {
	
	public int add( int a, int b ) 
	{
		return a + b;
	} // end method add
	
	public int subtract( int a, int b ) 
	{
		return a - b;
	} // end method subtract
	
	public int multiply( int a, int b ) 
	{
		return a * b;
	} // end method multiply
	
	public int intDivide( int a, int b ) 
	{
		int result = 0;
		try {
			result = a / b;
		} catch (ArithmeticException e) {
			System.err.println("ArithmeticException: " + e.getMessage());
		}
		return result;
	} // end method multiply

} // end class Calculator
